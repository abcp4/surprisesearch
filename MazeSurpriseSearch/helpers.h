#ifndef HELPERS_H
#define	HELPERS_H
#include <sstream>
//#include <iostream>
#include <vector>
#include <cstring>
#include <cmath> 

inline std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

inline double discretize(double x,long bins,double low, double high){
    double norm = x-low;
    double binsize = (high-low)/bins;
    int bin = (int)(norm/binsize);
    if(bin==bins){ bin--; }
    double result = (double)binsize*bin+binsize/2.0+low;
    return result;
}

inline long powerof2(int num){
    long x=1;
    if(num==0){ return 1; }
    for(int y=0;y<num;y++){ x*=2; }
    return x;
}

inline float gaussian(float value,float midpoint,float deviation){
    const float PI_F=3.14159265358979f;

    float exponent = -pow(value-midpoint,2)/(2*pow(deviation,2));
    //std::cout << "exponent="<< exponent;
    //float modifier = 1.f/(deviation*sqrt(2*PI_F));        // only if we need the surface to be 1, which we don't
    float modifier = 1.f;
    return modifier*exp(exponent);
}

inline double distance(double x1,double y1,double x2,double y2) {
    double dx=x1-x2;
    double dy=y1-y2;
    return sqrt(dx*dx+dy*dy);
}

inline float distance_points(const std::vector<float>& point1, const std::vector<float>& point2)
{
    return distance(point1[0], point1[1], point2[0], point2[1]);
}

inline float distance(std::vector<float> point1,std::vector<float> point2) {
    if(point1.size()!=2 && point2.size()!=2){ 
        std::cout << "wrong dimensions for 2D point";
        return -1;
    }
    return distance(point1[0],point1[1],point2[0],point2[1]);
}

inline float calculate_min(float dArray[], int iSize) {
    int iCurrMax = 0;
    for (int i = 1; i < iSize; ++i) {
        if (dArray[iCurrMax] > dArray[i]) {
            iCurrMax = i;
        }
    }
    return dArray[iCurrMax];
}

inline float calculate_max(float dArray[], int iSize) {
    int iCurrMin = 0;
    for (int i = 1; i < iSize; ++i) {
        if (dArray[iCurrMin] < dArray[i]) {
            iCurrMin = i;
        }
    }
    return dArray[iCurrMin];
}

inline float calculate_average(float dArray[], int iSize) {
    double dSum = dArray[0];
    for (int i = 1; i < iSize; ++i) {
        dSum += dArray[i];
    }
    return dSum/iSize;
}
inline float calculate_stdeviaton(float dArray[], int n)
{
    float mean=0.0, sum_deviation=0.0;
    int i;
    for(i=0; i<n;++i)
    {
        mean+=dArray[i];
    }
    mean=mean/n;
    for(i=0; i<n;++i)
    sum_deviation+=(dArray[i]-mean)*(dArray[i]-mean);
    return sqrt(sum_deviation/n);           
}
#endif	/* HELPERS_H */