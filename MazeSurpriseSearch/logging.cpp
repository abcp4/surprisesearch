//
// Created by Daniele.
//

#include "logging.h"
#include "noveltyset.h"

logging::logging() :generation(0){

}

logging::~logging() {

}

void logging::update_fittest(NEAT::Organism* org) {
    int allowed_size=5;
    if((int)fittest.size()<allowed_size) {
        if(org->noveltypoint!=NULL) {
            noveltyitem* x = new noveltyitem(*(org->noveltypoint));
            fittest.push_back(x);
            sort(fittest.begin(),fittest.end(),cmp_fit);
            reverse(fittest.begin(),fittest.end());
        } else {
            cout<<"WHY NULL?" << endl;
        }
    } else {
        if(org->noveltypoint->fitness > fittest.back()->fitness) {
            noveltyitem* x = new noveltyitem(*(org->noveltypoint));
            fittest.push_back(x);

            sort(fittest.begin(),fittest.end(),cmp_fit);
            reverse(fittest.begin(),fittest.end());

            delete fittest.back();
            fittest.pop_back();

        }
    }
}

void logging::update_fittest(NEAT::Population* pop) {

    sort(fittest.begin(),fittest.end(),cmp_fit);
    reverse(fittest.begin(),fittest.end());

}

void logging::serialize_fittest(char* fname) {
    ofstream outfile(fname);
    for(int i=0;i<(int)fittest.size();i++){
        fittest[i]->Serialize(outfile);
    }

    outfile.close();
}