#ifndef NOVELTYEXP_H
#define NOVELTYEXP_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <list>
#include <vector>
#include <algorithm>
#include <cmath>
#include <string>
#include "noveltyset.h"
#include "neat.h"
#include "network.h"
#include "population.h"
#include "organism.h"
#include "genome.h"
#include "species.h"
#include "datarec.h"
#include "maze.h"


using namespace std;

using namespace NEAT;

//Walker novelty steady-state 
//Population *maze_novelty_realtime(char* output_dir=NULL,const char* mazefile="maze.txt",bool useBorders=false,int param=-1);
Population *maze_novelty_realtime(void(*nov_eval)(noveltyarchive*,Organism*,Population*,Environment*,bool),char* output_dir=NULL,const char* mazefile="maze.txt",bool useBorders=false,int param=-1);
//int maze_novelty_realtime_loop(Population *pop);
int maze_novelty_realtime_loop(void(*nov_eval)(noveltyarchive*,Organism*,Population*,Environment*,bool),Population *pop);

noveltyitem* maze_novelty_map(Organism *org,data_record* record=NULL);

Population *maze_fitness_realtime(void(*fn_eval)(Organism*,Environment*),char* output_dir=NULL,const char* mazefile="maze.txt",bool useBorders=false,int param=-1);
int maze_fitness_realtime_loop(void(*fn_eval)(Organism*,Environment*),Population *pop);

Population *maze_surprise_realtime(AbstractSurpriseModel* surprise_model, char* output_dir=NULL,const char* mazefile="maze.txt",bool useBorders=false,int param=-1);
int maze_surprise_realtime_loop(AbstractSurpriseModel* surprise_model, Population *pop);

Population *maze_surprise_realtime(char* output_dir=NULL,const char* mazefile="maze.txt",bool useBorders=false,int param=-1);

int maze_surprise_realtime_loop(Population *pop);

double mazesimStep(Environment* newenv,Network *net,vector< vector<float> > &dc);
Environment* mazesimIni(Environment* tocopy,Network *net, vector< vector<float> > &dc);

#endif
