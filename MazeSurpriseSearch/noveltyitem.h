#ifndef NOVELTYITEM_H
#define	NOVELTYITEM_H

#include <math.h>
#include <vector>
#include <utility>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <cstdlib>

using namespace std;
using namespace NEAT;

struct SurpriseItem
{
public:

    vector< vector<float> > parents;

    vector< vector<float> > grandparents;

};

//a novelty item is a "stake in the ground" i.e. a novel phenotype
class noveltyitem {
public:
bool added;
int indiv_number;
//we can keep track of genotype & phenotype of novel item
NEAT::Genome* genotype;
NEAT::Network* phenotype;

//used to collect data
vector< vector<float> > data;

//future use
float age;

//used for analysis purposes
float novelty;
float fitness;
float generation;

//this will write a novelty item to file
bool Serialize(ofstream& ofile)
{
    genotype->print_to_file(ofile);
    SerializeNoveltyPoint(ofile);
    return true;
}

//serialize the novelty point itself to file
bool SerializeNoveltyPoint(ofstream& ofile) {
    ofile << "/* Novelty: " << novelty << " Fitness: " << fitness << " Generation: " << generation << " Indiv: " << indiv_number << " */" << endl;
    ofile << "/* Point:";
    for(int i=0;i<(int)data.size();i++){
        for(int j=0;j<(int)data[i].size();j++){
            ofile << " " << data[i][j];
        }
    }
    ofile << " */" << endl;
    return true;
}

//copy constructor
//noveltyitem(const noveltyitem& item);
noveltyitem(const noveltyitem& item){
    added=item.added;
    genotype=new Genome(*(item.genotype));
    phenotype=new Network(*(item.phenotype));
    age=item.age;
    fitness=item.fitness;
    novelty=item.novelty;
    generation=item.generation;
    indiv_number=item.indiv_number;
    for(int i=0;i<(int)item.data.size();i++){
        vector<float> temp;
        for(int j=0;j<(int)item.data[i].size();j++){
            temp.push_back(item.data[i][j]);
        }
        data.push_back(temp);		
    }
}

//initialize...
noveltyitem() {
    added=false;
    genotype=NULL;
    phenotype=NULL;
    age=0.0;
    generation=0.0;
    indiv_number=(-1);
}

~noveltyitem() {
    if(genotype)
        delete genotype;
    if(phenotype)
        delete phenotype;
}


};

#endif	/* NOVELTYITEM_H */

