#include "noveltyset.h"
#include "population.h"
#include "organism.h"
#include "noveltyitem.h"

//#define NO_ARCHIVE

//std::random_device rand_dev;
//std::mt19937 generator(rand_dev());
//std::uniform_real_distribution<double> distribution(0.0,1.0);

//for sorting by novelty
bool cmp(const noveltyitem *a, const noveltyitem* b){
	return a->novelty < b->novelty;
}

//for sorting by fitness
bool cmp_fit(const noveltyitem *a, const noveltyitem *b){
	return a->fitness < b->fitness;
}

//evaluate the novelty of the whole population
void noveltyarchive::evaluate_population(Population* pop,bool fitness){
	Population *p = (Population*)pop;
	vector<Organism*>::iterator it;
	for(it=p->organisms.begin();it<p->organisms.end();it++){
		evaluate_individual((*it),pop,fitness);
	}
}

//evaluate the novelty of a single individual
void noveltyarchive::evaluate_individual(Organism* ind,Population* pop,bool fitness){
	float result;
	if(fitness){	//assign fitness according to average novelty
		result = novelty_avg_nn(ind->noveltypoint,-1,false,pop);	// no smoothing when you assign novelty as 'fitness'
		ind->fitness = result;
		//cout << "  ind->fitness = " << result << endl;
	} else {		//consider adding a point to archive based on dist to nearest neighbor
		result = novelty_avg_nn(ind->noveltypoint,1,false);
		ind->noveltypoint->novelty=result;
		//cout << "  ind->noveltypoint->novelty = " << result << endl;
		if(add_to_novelty_archive(result)){
			add_novel_item(ind->noveltypoint);
		}
	}
}

void noveltyarchive::evaluate_population(Population* pop,Environment* env,void (*nov_eval)(noveltyarchive*,Organism*,Population*,Environment*,bool),bool fitness){
	Population *p = (Population*)pop;
	vector<Organism*>::iterator it;
	for(it=p->organisms.begin();it<p->organisms.end();it++){
		nov_eval(this,(*it),pop,env,fitness);
	}
}
/*
void noveltyarchive::naive_evaluate_population(Population* pop,Environment* env,bool fitness){
	Population *p = (Population*)pop;
	vector<Organism*>::iterator it;
	for(it=p->organisms.begin();it<p->organisms.end();it++){
		naive_evaluate_individual((*it),pop,env,fitness);
	}
}

void noveltyarchive::naive_evaluate_individual(Organism* ind,Population* pop,Environment* env,bool fitness){
	float result;
	if(fitness){	//assign fitness according to average novelty
		result = novelty_avg_nn(ind->noveltypoint,-1,false,pop);	// no smoothing when you assign novelty as 'fitness'
		ind->fitness = result;
	} else {		//consider adding a point to archive based on dist to nearest neighbor
		result = novelty_avg_nn(ind->noveltypoint,1,false);
		ind->noveltypoint->novelty=result;
		if(add_to_novelty_archive(result)){
			add_novel_item(ind->noveltypoint);
		}
	}
}

//evaluate the novelty of the whole population
void noveltyarchive::mcns_evaluate_population(Population* pop,Environment* env,bool fitness){
	Population *p = (Population*)pop;
	vector<Organism*>::iterator it;
	for(it=p->organisms.begin();it<p->organisms.end();it++){
		mcns_evaluate_individual((*it),pop,env,fitness);
	}
}
//evaluate the novelty of a single individual
void noveltyarchive::mcns_evaluate_individual(Organism* ind,Population* pop,Environment* env,bool fitness){
	float result;
	if(fitness){	//assign fitness according to average novelty
		if(env->outOfBounds(ind->noveltypoint->data[0])){ 
			ind->fitness = 0; 
		} else {
			result = novelty_avg_nn(ind->noveltypoint,-1,false,pop);
			ind->fitness = result;
		}
	} else {		//consider adding a point to archive based on dist to nearest neighbor
		if(false){	// I could be wrong here, but I assume no infeasible individuals go into the archive
			if(!env->outOfBounds(ind->noveltypoint->data[0])){	
				result = novelty_avg_nn(ind->noveltypoint,1,false);
				ind->noveltypoint->novelty=result;
				if(add_to_novelty_archive(result)){
					add_novel_item(ind->noveltypoint);
				}
			}
		} else {	// if I am wrong, I've put up an alternative calculation for novel archive insertion
			if(env->outOfBounds(ind->noveltypoint->data[0])){	
				ind->noveltypoint->novelty=0;
			} else {
				result = novelty_avg_nn(ind->noveltypoint,1,false);
				ind->noveltypoint->novelty=result;
			}
			if(add_to_novelty_archive(result)){
				add_novel_item(ind->noveltypoint);
			}
		}
	}
}

//evaluate the novelty of the whole population
void noveltyarchive::penalty_evaluate_population(Population* pop,Environment* env,bool fitness){
	Population *p = (Population*)pop;
	vector<Organism*>::iterator it;
	for(it=p->organisms.begin();it<p->organisms.end();it++){
		penalty_evaluate_individual((*it),pop,env,fitness);
	}
}
//evaluate the novelty of a single individual
void noveltyarchive::penalty_evaluate_individual(Organism* ind,Population* pop,Environment* env,bool fitness){
	float result;
	if(fitness){	//assign fitness according to average novelty	
		result = novelty_avg_nn(ind->noveltypoint,-1,false,pop);
		if(env->outOfBounds(ind->noveltypoint->data[0])){ 
			result -= env->distFromBounds(ind->noveltypoint->data[0]);
		}
		ind->fitness = result;
	} else {		//consider adding a point to archive based on dist to nearest neighbor
		result = novelty_avg_nn(ind->noveltypoint,1,false);
		if(env->outOfBounds(ind->noveltypoint->data[0])){ 
			result -= env->distFromBounds(ind->noveltypoint->data[0]);
		}
		ind->noveltypoint->novelty=result;
		if(add_to_novelty_archive(result)){
			add_novel_item(ind->noveltypoint);
		}
	}
}
*/
// --------------------------------------------
// COPIED FROM HEADER
// --------------------------------------------

//the novelty archive contains all of the novel items we have encountered thus far
//Using a novelty metric we can determine how novel a new item is compared to everything
//currently in the novelty set 
noveltyarchive::noveltyarchive(float threshold,float (*nm)(noveltyitem*,noveltyitem*),bool rec, int neigh) {
	//how many nearest neighbors to consider for calculating novelty score?
	if(neigh<=0){ 
		neighbors=NEAT::nearest_neighbors;
	} else {
		neighbors=neigh;
	}
        cout << "neigh " << neighbors << endl;
        
	generation=0;
	time_out=0; //used for adaptive threshold
	novelty_threshold=threshold;
	novelty_metric=nm; //set the novelty metric via function pointer
	novelty_floor=0.25; //lowest threshold is allowed to get
	record=rec;
	this_gen_index=ARCHIVE_SEED_AMOUNT;
	hall_of_fame=false;
	threshold_add=true;

	if(record) { datafile = new ofstream("runresults.dat"); }
}

noveltyarchive::~noveltyarchive() {
	if(record) { datafile->close(); }
	//probably want to delete all the noveltyitems at this point

	cout << "cleaning fittest" << endl;
	for (auto nov_item:fittest)
	{
		delete nov_item;
	}
}
	
float noveltyarchive::get_threshold() { return novelty_threshold; }
int noveltyarchive::get_set_size() { return (int)novel_items.size(); }
	
//add novel item to archive
void noveltyarchive::add_novel_item(noveltyitem* item,bool aq) {
        
#ifdef NO_ARCHIVE
        return;
#endif
        
	item->added=true;
	item->generation=generation;
        novel_items.push_back(item);
	if(aq){ add_queue.push_back(item); }
}
void noveltyarchive::add_randomly(Population* pop) {
	for(int i=0;i<(int)pop->organisms.size();i++) {
		if (((float)rand()/RAND_MAX)<(0.0005)) {
			noveltyitem* newitem = new noveltyitem(*pop->organisms[i]->noveltypoint);
			if(newitem->novelty > MIN_ACCEPTABLE_NOVELTY){
				add_novel_item(newitem,false);
			}
			else delete newitem;
		}
	}
}

noveltyitem* noveltyarchive::get_item(int i) { return novel_items[i]; }

//maintain list of fittest organisms so far
void noveltyarchive::update_fittest(Organism* org) {
	int allowed_size=5;
	if((int)fittest.size()<allowed_size) {
		if(org->noveltypoint!=NULL) {
			noveltyitem* x = new noveltyitem(*(org->noveltypoint));
			fittest.push_back(x);
			sort(fittest.begin(),fittest.end(),cmp_fit);
			reverse(fittest.begin(),fittest.end());
		} else {
			cout<<"WHY NULL?" << endl;
		}
	} else {
		if(org->noveltypoint->fitness > fittest.back()->fitness) {
			noveltyitem* x = new noveltyitem(*(org->noveltypoint));
			fittest.push_back(x);

			sort(fittest.begin(),fittest.end(),cmp_fit);
			reverse(fittest.begin(),fittest.end());

			delete fittest.back();
			fittest.pop_back();

		}
	}
}

//resort fittest list
void noveltyarchive::update_fittest(Population* pop) {
	sort(fittest.begin(),fittest.end(),cmp_fit);
	reverse(fittest.begin(),fittest.end());
}

//write out fittest list
void noveltyarchive::serialize_fittest(char *fn) {
	ofstream outfile(fn);
	for(int i=0;i<(int)fittest.size();i++){
		fittest[i]->Serialize(outfile);
	}
        
        //printing size of archive for logging purpose
        
        outfile << "/* Archive Size: " << novel_items.size() << " */" << endl;
        
	outfile.close();
}

//adjust dynamic novelty threshold depending on how many have been added to
//archive recently
void noveltyarchive::add_pending() {
	if(record) {
		(*datafile) << novelty_threshold << " " << add_queue.size() << endl;
	}

	if(hall_of_fame) {		
		if(add_queue.size()==1){ 
			time_out++;
		} else {
			time_out=0;
		}
	} else {
		if(add_queue.size()==0){
			time_out++;
		} else {
			time_out=0;
		}
	}

	//if no individuals have been added for 10 generations
	//lower threshold
	if(time_out==10) {
			novelty_threshold*=0.95;
			if(novelty_threshold<novelty_floor){ 
				novelty_threshold=novelty_floor; 
			}
			time_out=0;
	}

	//if more than four individuals added this generation
	//raise threshold
	if(add_queue.size()>4){ 
		novelty_threshold*=1.2; 
	}

	add_queue.clear();

	this_gen_index = novel_items.size();
}

//criteria for adding to the archive
bool noveltyarchive::add_to_novelty_archive(float novelty) {
#ifdef NO_ARCHIVE
        return false;
#endif
	if(novelty>novelty_threshold){
		return true;
	} else {
		return false;
	}
}

//only used in generational model (obselete)
void noveltyarchive::end_of_gen() {
	generation++;
	if(threshold_add) {
		find_novel_items(true);
	}

	if(hall_of_fame) {
		find_novel_items(false);

		sort(current_gen.begin(),current_gen.end(),cmp);
		reverse(current_gen.begin(),current_gen.end());

		add_novel_item(current_gen[0]);
	}

	clean_gen();
	add_pending();
}

//steady-state end of generation call (every so many indivudals)
void noveltyarchive::end_of_gen_steady(Population* pop) {	
	generation++;
	add_pending();
	vector<Organism*>::iterator cur_org;
}

void noveltyarchive::clean_gen() {
	vector<noveltyitem*>::iterator cur_item;

	bool datarecord=true;	

	stringstream filename("");
	filename << "novrec/out" << generation << ".dat";
	ofstream outfile(filename.str().c_str());
	cout << filename.str() << endl;

	for(cur_item=current_gen.begin();cur_item!=current_gen.end();cur_item++) {
		if(datarecord) {
			(*cur_item)->SerializeNoveltyPoint(outfile);
		}
		if(!(*cur_item)->added) {
			delete (*cur_item);
		}
	}
	current_gen.clear();
}

//see if there are any individuals in current generation
//that need to be added to the archive (obselete)
void noveltyarchive::find_novel_items(bool add=true) {
	vector<noveltyitem*>::iterator cur_item;
	for(cur_item=current_gen.begin();cur_item!=current_gen.end();cur_item++) {
		float novelty = test_novelty((*cur_item));
		(*cur_item)->novelty = novelty;
		if(add && add_to_novelty_archive(novelty)){
			add_novel_item(*cur_item);
		}
	}
}

//add an item to current generation (obselete)
void noveltyarchive::add_to_generation(noveltyitem* item) {
	current_gen.push_back(item);
}

//nearest neighbor novelty score calculation
float noveltyarchive::novelty_avg_nn(noveltyitem* item,int neigh,bool ageSmooth,Population* pop) {
	vector<sort_pair> novelties;
	if(pop){
		novelties = map_novelty_pop(novelty_metric,item,pop);
	} else {
		novelties = map_novelty(novelty_metric,item);
	}
	sort(novelties.begin(),novelties.end());

	float density=0.0;
	int len=novelties.size();
	float sum=0.0;
	float weight=0.0;


	if(neigh==-1){ neigh=neighbors; }

	if(len<ARCHIVE_SEED_AMOUNT) {		
		item->age=1.0;
		add_novel_item(item);	
	} else {
		len=neigh;
		if((int)novelties.size()<len){
			len=novelties.size();
		}
		int i=0;

		while(weight<neigh && i<(int)novelties.size()) {
			float term = novelties[i].first;
			float w = 1.0;

			if(ageSmooth){
				float age=(novelties[i].second)->age;
				w=1.0-pow((float)0.95,age);
			}

			sum+=term*w;
			weight+=w;
			i++;
		}

		if(weight!=0){ density = sum/weight; }	// average
	}	

	item->novelty=density;
	item->generation=generation;
	return density;
}

//fitness = avg distance to k-nn in novelty space
float noveltyarchive::test_fitness(noveltyitem* item){
	return novelty_avg_nn(item,-1,false);
}

float noveltyarchive::test_novelty(noveltyitem* item){
	return novelty_avg_nn(item,1,false);
}



//write out archive
bool noveltyarchive::Serialize(char* fname) {
	ofstream outfile;
	outfile.open(fname);
	bool res= Serialize(outfile);
	outfile.close();
	return res;
}

//write out archive
bool noveltyarchive::Serialize(ofstream& ofile) {
	for(int i=0;i<(int)novel_items.size();i++){
		novel_items[i]->Serialize(ofile);
	}
	return true;		
}


// -------------------------------------------
// NON-CLASS FUNCTION POINTERS
// -------------------------------------------

void naive_evaluate_individual(noveltyarchive* archive, Organism* ind,Population* pop,Environment* env,bool fitness){
	float result;
	if(fitness){	//assign fitness according to average novelty
		result = archive->novelty_avg_nn(ind->noveltypoint,-1,false,pop);	// no smoothing when you assign novelty as 'fitness'
		ind->fitness = result;
	} else {		//consider adding a point to archive based on dist to nearest neighbor
		result = archive->novelty_avg_nn(ind->noveltypoint,1,false);
		ind->noveltypoint->novelty=result;
		if(archive->add_to_novelty_archive(result)){
			archive->add_novel_item(ind->noveltypoint);
		}
	}
}

//evaluate the novelty of a single individual
void mcns_evaluate_individual(noveltyarchive* archive,Organism* ind,Population* pop,Environment* env,bool fitness){
	float result;
	if(fitness){	//assign fitness according to average novelty
		if(env->outOfBounds(ind->noveltypoint->data[0])){ 
			ind->fitness = 0; 
		} else {
			result = archive->novelty_avg_nn(ind->noveltypoint,-1,false,pop);
			ind->fitness = result;
		}
	} else {		//consider adding a point to archive based on dist to nearest neighbor
		if(false){	// I could be wrong here, but I assume no infeasible individuals go into the archive
			if(!env->outOfBounds(ind->noveltypoint->data[0])){	
				result = archive->novelty_avg_nn(ind->noveltypoint,1,false);
				ind->noveltypoint->novelty=result;
				if(archive->add_to_novelty_archive(result)){
					archive->add_novel_item(ind->noveltypoint);
				}
			}
		} else {	// if I am wrong, I've put up an alternative calculation for novel archive insertion
			if(env->outOfBounds(ind->noveltypoint->data[0])){	
				ind->noveltypoint->novelty=0;
			} else {
				result = archive->novelty_avg_nn(ind->noveltypoint,1,false);
				ind->noveltypoint->novelty=result;
			}
			if(archive->add_to_novelty_archive(result)){
				archive->add_novel_item(ind->noveltypoint);
			}
		}
	}
}

void penalty_evaluate_individual(noveltyarchive* archive, Organism* ind,Population* pop,Environment* env,bool fitness){
	// DON'T FORGET THAT WE TRY TO MAXIMIZE THE DISTANCE WITH NEAREST NEIGHBORS
	float result;
	if(fitness){	//assign fitness according to average novelty	
		result = archive->novelty_avg_nn(ind->noveltypoint,-1,false,pop);
		if(env->outOfBounds(ind->noveltypoint->data[0])){ 
			result -= env->distFromBounds(ind->noveltypoint->data[0]);
			if(result<0.00001){ result = 0.00001; }
		}
		ind->fitness = result;
	} else {		//consider adding a point to archive based on dist to nearest neighbor
		result = archive->novelty_avg_nn(ind->noveltypoint,1,false);
		if(env->outOfBounds(ind->noveltypoint->data[0])){ 
			result -= env->distFromBounds(ind->noveltypoint->data[0]);
			if(result<0.00001){ result = 0.00001; }
		}
		ind->noveltypoint->novelty=result;
		if(archive->add_to_novelty_archive(result)){
			archive->add_novel_item(ind->noveltypoint);
		}
	}
}

void naive_evaluate_fitness(Organism* individual,Environment* env){
	float sim_fn = individual->noveltypoint->fitness;
	individual->fitness = sim_fn;
}
void death_evaluate_fitness(Organism* individual,Environment* env){
	float sim_fn = individual->noveltypoint->fitness;
	if(env->outOfBounds(individual->noveltypoint->data[0])){
		sim_fn = 0;
	}
	individual->fitness = sim_fn;
}

void penalty_evaluate_fitness(Organism* individual,Environment* env){
	float sim_fn = individual->noveltypoint->fitness;
	if(env->outOfBounds(individual->noveltypoint->data[0])){
		sim_fn -= env->distFromBounds(individual->noveltypoint->data[0]);
		if(sim_fn<0.00001){ sim_fn = 0.00001; }
	}
	individual->fitness = sim_fn;
}


// -----------------------------------------
// SURPRISE ADDITIONS
// -----------------------------------------
#include "surprise_math.h"

void surprise_evaluate_population(Population* pop, Environment* env, AbstractSurpriseModel* surprise_model,
								  void (*surprise_eval)(AbstractSurpriseModel* surprise_model, Organism* ind,Population* pop,Environment* env))
{
	vector<Organism*>::iterator it;
	for(it = pop->organisms.begin(); it < pop->organisms.end(); it++ ){
		surprise_eval(surprise_model, (*it), pop, env);
	}
}

void surprise_evaluate_individual(AbstractSurpriseModel* surprise_model, Organism* ind,Population* pop,Environment* env){
	
	ind->fitness = surprise_model->evaluateIndividual(ind, pop);
}
