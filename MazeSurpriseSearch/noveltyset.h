#ifndef NVSET_H
#define NVSET_H

#include <math.h>
#include <vector>
#include <utility>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <cstdlib>

#include "population.h"
#include "noveltyitem.h"

#define ARCHIVE_SEED_AMOUNT 1

class AbstractSurpriseModel;

//different comparison functions used for sorting
bool cmp(const noveltyitem *a, const noveltyitem *b);
bool cmp_fit(const noveltyitem *a, const noveltyitem *b);
 
#define MIN_ACCEPTABLE_NOVELTY 0.005

//the novelty archive contains all of the novel items we have encountered thus far
//Using a novelty metric we can determine how novel a new item is compared to everything
//currently in the novelty set 
class noveltyarchive {
	
private:
	//are we collecting data?
	bool record;

	ofstream *datafile;
	ofstream *novelfile;
	typedef pair<float, noveltyitem*> sort_pair;
	//all the novel items we have found so far
	vector<noveltyitem*> novel_items;
	vector<noveltyitem*> fittest;

	//current generation novelty items
	vector<noveltyitem*> current_gen;

	//novel items waiting addition to the set pending the end of the generation 
	vector<noveltyitem*> add_queue;
	//the measure of novelty
	float (*novelty_metric)(noveltyitem*,noveltyitem*);
	//minimum threshold for a "novel item"
	float novelty_threshold;
	float novelty_floor;
	//counter to keep track of how many gens since we've added to the archive
	int time_out;
	//parameter for how many neighbors to look at for N-nearest neighbor distance novelty
	int neighbors;
	//radius for SOG-type (not currently used)
	float radius;
	int this_gen_index;
	
	//hall of fame mode, add an item each generation regardless of threshold
	bool hall_of_fame;
	//add new items according to threshold
	bool threshold_add;
public:
	//constructor
	noveltyarchive(float threshold,float (*nm)(noveltyitem*,noveltyitem*),bool rec=true,int neigh=15);
	~noveltyarchive();
	
public:
        //current generation
	int generation;
        
	float get_threshold();
	int get_set_size();
	
	//add novel item to archive
	void add_novel_item(noveltyitem* item,bool aq=true);

	//not currently used
	void add_randomly(Population* pop);
	
	noveltyitem *get_item(int i);
	
	//re-evaluate entire population for novelty
	void evaluate_population(Population* pop,bool fitness=true);
	//evaluate single individual for novelty
        void evaluate_individual(Organism* individual,Population* pop,bool fitness=true);
        // function pointer trial
        void evaluate_population(Population* pop,Environment* env,void (*nov_eval)(noveltyarchive*,Organism*,Population*,Environment*,bool),bool fitness);
        void evaluate_population(Population* pop,Environment* env,AbstractSurpriseModel* surprise_model,void (*nov_eval)(noveltyarchive*,AbstractSurpriseModel*,Organism*,Population*,Environment*,bool),bool fitness);
        //function pointer trials
        /*
	void naive_evaluate_population(Population* pop,Environment* env,bool fitness=true);
	void naive_evaluate_individual(Organism* individual,Population* pop,Environment* env,bool fitness=true);
	void mcns_evaluate_population(Population* pop,Environment* env,bool fitness=true);
        void mcns_evaluate_individual(Organism* individual,Population* pop,Environment* env,bool fitness=true);
        void penalty_evaluate_population(Population* pop,Environment* env,bool fitness=true);
        void penalty_evaluate_individual(Organism* individual,Population* pop,Environment* env,bool fitness=true);
        */
        //maintain list of fittest organisms so far
	void update_fittest(Organism* org);
	
	//resort fittest list
	void update_fittest(Population* pop);
	//write out fittest list
	void serialize_fittest(char *fn);
	//adjust dynamic novelty threshold depending on how many have been added to
	//archive recently
	void add_pending();
	//criteria for adding to the archive
	bool add_to_novelty_archive(float novelty);
	//only used in generational model (obselete)
	void end_of_gen();
	//steady-state end of generation call (every so many indivudals)
	void end_of_gen_steady(Population* pop);
	void clean_gen();
	//see if there are any individuals in current generation
	//that need to be added to the archive (obselete)
	void find_novel_items(bool add);
	
	//add an item to current generation (obselete)
	void add_to_generation(noveltyitem* item);
	//nearest neighbor novelty score calculation
	float novelty_avg_nn(noveltyitem* item,int neigh=-1,bool ageSmooth=false,Population* pop=NULL);
	//fitness = avg distance to k-nn in novelty space
	float test_fitness(noveltyitem* item);
	float test_novelty(noveltyitem* item);

	//vector<sort_pair> map_novelty(float (*nov_func)(noveltyitem*,noveltyitem*),noveltyitem* newitem);
	//vector<sort_pair> map_novelty_pop(float (*nov_func)(noveltyitem*,noveltyitem*),noveltyitem* newitem, Population* pop);
	
        //map the novelty metric across the archive
        vector<sort_pair> map_novelty(float (*nov_func)(noveltyitem*,noveltyitem*),noveltyitem* newitem){
            vector<sort_pair> novelties;
            for(int i=0;i<(int)novel_items.size();i++) {			
                novelties.push_back(make_pair((*novelty_metric)(novel_items[i],newitem),novel_items[i])); 
            }
            return novelties;
        }

        //map the novelty metric across the archive + current population
        vector<sort_pair> map_novelty_pop(float (*nov_func)(noveltyitem*,noveltyitem*),noveltyitem* newitem, Population* pop) {
            vector<sort_pair> novelties;
            for(int i=0;i<(int)novel_items.size();i++) {
                novelties.push_back(make_pair((*novelty_metric)(novel_items[i],newitem),novel_items[i]));
            }
            for(int i=0;i<(int)pop->organisms.size();i++) {
                novelties.push_back(make_pair((*novelty_metric)(pop->organisms[i]->noveltypoint,newitem),
                pop->organisms[i]->noveltypoint));
            }
            return novelties;
        }

	//map the novelty metric across the archive + current population
	
	//write out archive
	bool Serialize(char* fname);
	
	//write out archive
	bool Serialize(ofstream& ofile);
};


void naive_evaluate_individual(noveltyarchive* archive,Organism* individual,Population* pop,Environment* env,bool fitness=true);
void mcns_evaluate_individual(noveltyarchive* archive,Organism* individual,Population* pop,Environment* env,bool fitness=true);
void penalty_evaluate_individual(noveltyarchive* archive,Organism* individual,Population* pop,Environment* env,bool fitness=true);

void naive_evaluate_fitness(Organism* individual,Environment* env);
void death_evaluate_fitness(Organism* individual,Environment* env);
void penalty_evaluate_fitness(Organism* individual,Environment* env);

void surprise_evaluate_population(Population* pop, Environment* env, AbstractSurpriseModel* surprise_model,
								  void (*surprise_eval)(AbstractSurpriseModel* surprise_model, Organism* ind,Population* pop,Environment* env));

void surprise_evaluate_individual(AbstractSurpriseModel* surprise_model,Organism* individual,Population* pop,Environment* env);


#endif
