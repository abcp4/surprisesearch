#include "surprise_math.h"

#include "population.h"
#include "organism.h"
#include "species.h"
#include "noveltyitem.h"

#include "helpers.h"

#include <vector>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <random>

#include "linreg.h"

#include "Matrix.h"
#include <stdbool.h>
#include <math.h>

#include <chrono>
#include <cfloat>

using namespace chrono;

// -------------------------------------------------
// CLASS CONTENTS
// -------------------------------------------------

KMeansClusteringModel::KMeansClusteringModel(int num_cluster,  int neigh):
            num_cluster(num_cluster), neigh(neigh)
{
    //sanity check
    if(neigh > num_cluster)
    {
        cout << "ERROR: NEIGH > NUM CLUSTER !! " << "setting neigh = num_cluster" << endl;
        neigh = num_cluster;
    }
}

KMeansClusteringModel::~KMeansClusteringModel(){
    
    if(centroid_savefile){ centroid_savefile.close(); }
    if(pred_centroid_savefile){ pred_centroid_savefile.close(); }
}

void KMeansClusteringModel::initLogs(char* output_dir){
        
	// initialize logs
	char cName[100];
	sprintf(cName,"%scentroids.txt",output_dir);
	centroid_savefile.open(cName);
        
	sprintf(cName,"%spredcentroids.txt",output_dir);
	pred_centroid_savefile.open(cName);

        
	sprintf(cName, "%smodel.txt", output_dir);
	model_savefile.open(cName);
        
	model_savefile << "K-means clustering " << num_cluster;

	model_savefile << " neighborhood: " << neigh << endl;

	model_savefile.close();
}

void KMeansClusteringModel::assignMaze(Environment* env){ }

void KMeansClusteringModel::updateValues(NEAT::Population* pop) {

	vector<int> counts_cluster(num_cluster, 0);

	vector<double> centroids_x(num_cluster, 0);
	vector<double> centroids_y(num_cluster, 0);

	vector<double> best_centroids_x(num_cluster, 0);
	vector<double> best_centroids_y(num_cluster, 0);

	vector<double> old_centroids_x(num_cluster, 0);
	vector<double> old_centroids_y(num_cluster, 0);

	vector<double> temp_centroids_x(num_cluster, 0);
	vector<double> temp_centroids_y(num_cluster, 0);

	vector<double> xcoordinates;
	vector<double> ycoordinates;

	vector<int> labels(NEAT::pop_size, 0);

	vector<int> best_labels(NEAT::pop_size, 0);

	/*****************
    ** prediction phase
    *****************/

	if (prev_centroids.size() >= 2*num_cluster) {

		vector<vector<double> > centroids_to_predict;

		predictedcentroids.clear();

		for (int i = 0; i < num_cluster; i++) {
			centroids_to_predict.clear();

			centroids_to_predict.emplace_back(prev_centroids[prev_centroids.size() - num_cluster * 2 + i]);
			centroids_to_predict.emplace_back(prev_centroids[prev_centroids.size() - num_cluster + i]);
			predictedcentroids.emplace_back(predictNextPoint(centroids_to_predict));
		}

		if (pred_centroid_savefile) {

			for(int index = 0; index < predictedcentroids.size(); ++index)
			{
				pred_centroid_savefile << predictedcentroids[index][0] << " " << predictedcentroids[index][1] << " " << index << endl;
			}
		}
	}
	else {
		if (pred_centroid_savefile) {
			pred_centroid_savefile << endl;
		}
	}

	/*
     * Kmeans phase
     */


	for (int i = 0; i < pop->species.size(); i++) {
		for (int j = 0; j < pop->species[i]->organisms.size(); j++) {
			xcoordinates.emplace_back(pop->species[i]->organisms[j]->noveltypoint->data[0][0]);
			ycoordinates.emplace_back(pop->species[i]->organisms[j]->noveltypoint->data[0][1]);
		}
	}

	vector<double> currentcentroid = calculateCentroid(xcoordinates, ycoordinates);

	/* preprocessing: center data in the origin*/
	for (int h = 0; h < xcoordinates.size(); ++h) {
		xcoordinates[h] -= currentcentroid[0];
		ycoordinates[h] -= currentcentroid[1];
	}

	/*after first gen */
	if (prev_centroids.size() >= num_cluster) {

		/*seed from previous generation kmeans*/
		for (int i = 0; i < num_cluster; ++i) {
			centroids_x[i] = prev_centroids[prev_centroids.size() - num_cluster + i][0];
			centroids_y[i] = prev_centroids[prev_centroids.size() - num_cluster + i][1];

			/*subtract mean*/
			centroids_x[i] -= currentcentroid[0];
			centroids_y[i] -= currentcentroid[1];
		}
	}
	else { //first gen -> random initialization of centroids

		vector<int> indexes(xcoordinates.size(), 0);

		for (int i = 0; i < xcoordinates.size(); ++i) {
			indexes[i] = i;
		}

		random_shuffle(indexes.begin(), indexes.end());

		for (int i = 0; i < num_cluster; ++i) {
			centroids_x[i] = xcoordinates[indexes[i]];
			centroids_y[i] = ycoordinates[indexes[i]];
		}
	}

	int iter_counter = 0;
	double squared_norm = 0.0;
	double best_error = numeric_limits<double>::max();
	double error = 0.0;

	do {
		/* save error from last step */
		error = 0.0;
		squared_norm = 0.0;

		/* clear old counts and temp centroids */
		for (int i = 0; i < num_cluster; i++) {

			counts_cluster[i] = 0;

			temp_centroids_x[i] = 0.0;
			temp_centroids_y[i] = 0.0;

		}

		/*safe check- > all points assigned*/
		std::fill(labels.begin(), labels.end(), -1);

        double min_distance, dist;

		for (int h = 0; h < xcoordinates.size(); ++h) {

			/* identify the closest cluster to the point h */
			min_distance = numeric_limits<double>::max();
			dist = 0;

			for (int i = 0; i < num_cluster; ++i) {

				dist = distance(xcoordinates[h], ycoordinates[h], centroids_x[i], centroids_y[i]);

				if (dist < min_distance) {
					labels[h] = i;
					min_distance = dist;
				}
			}

			/* update size and temp centroid of the destination cluster */

			temp_centroids_x[labels[h]] += xcoordinates[h];
			temp_centroids_y[labels[h]] += ycoordinates[h];


			counts_cluster[labels[h]]++;

			/* update standard error */
			error += min_distance;
		}

		for (int i = 0; i < num_cluster; i++) { /* update all centroids */

			old_centroids_x[i] = centroids_x[i];
			old_centroids_y[i] = centroids_y[i];

			if (counts_cluster[i]) {
				centroids_x[i] = (double) temp_centroids_x[i] / counts_cluster[i];
				centroids_y[i] = (double) temp_centroids_y[i] / counts_cluster[i];
			}
		}

		for (int i = 0; i < num_cluster; i++) {
			squared_norm += (centroids_x[i] - old_centroids_x[i]) * (centroids_x[i] - old_centroids_x[i]);
			squared_norm += (centroids_y[i] - old_centroids_y[i]) * (centroids_y[i] - old_centroids_y[i]);
		}

		if (error < best_error) {
			for (int i = 0; i < num_cluster; i++) {
				best_centroids_x[i] = centroids_x[i];
				best_centroids_y[i] = centroids_y[i];
			}

			for (int i = 0; i < labels.size(); i++) {
				best_labels[i] = labels[i];
			}

			best_error = error;
		}

		squared_norm = sqrt(squared_norm);

		iter_counter++;

	}
	while (squared_norm > FLT_EPSILON && iter_counter < 300);


	// save best centroids found
	for (int i = 0; i < num_cluster; i++) {

		// shift in the original space (add mean)

		best_centroids_x[i] += currentcentroid[0];
		best_centroids_y[i] += currentcentroid[1];

		prev_centroids.emplace_back(vector<double>() = {best_centroids_x[i], best_centroids_y[i]});

	}

	if (centroid_savefile) {

		for(int index = 0; index < best_centroids_x.size(); ++index)
		{
			centroid_savefile << best_centroids_x[index] << " " << best_centroids_y[index] << " " << endl;
		}
	}

	//erase old centroids (case k too high)
	if (prev_centroids.size() > num_cluster * 2) {
		prev_centroids.erase(prev_centroids.begin(),
							 prev_centroids.begin() + prev_centroids.size() - num_cluster * 2);
	}
}

float KMeansClusteringModel::evaluateIndividual(vector<float> &position){

    double min_dist = numeric_limits<double>::max();
	double dist;
    
    if(prev_centroids.size() >= 2*num_cluster && predictedcentroids.size() >= num_cluster){	// sanity for predictedcentroid
            
		if(neigh <= 1){ //no neighborhood

			for(auto it = predictedcentroids.begin(); it != predictedcentroids.end(); it++)
			{
				dist = distance(position[0], position[1], (*it)[0], (*it)[1]);

				if(dist < min_dist) {
					min_dist = dist;
				}
			}


			return min_dist;
		}
		else //yes neighborhood
		{
			vector<double> distances;

			for(auto it = predictedcentroids.begin(); it != predictedcentroids.end(); it++)
			{
				dist = distance(position[0], position[1], (*it)[0], (*it)[1]);
				distances.emplace_back(dist);
			}

			sort(distances.begin(), distances.end());

			dist = 0.0;

			for(int i = 0; i < neigh; i++)
			{
				dist += distances[i];
			}

			return dist/neigh;
		}
	}
    else
    {
        return 1;
    }
}

float KMeansClusteringModel::evaluateIndividual(NEAT::Organism* org, NEAT::Population* pop){
	return evaluateIndividual(org->noveltypoint->data[0]);
}

// -------------------------------------------------
// BASIC MATH OPERATORS
// -------------------------------------------------

vector<double> predictNextPoint(double x1, double y1, double x2, double y2)
{
    vector< vector<double> > points;
    
    vector<double> temp1, temp2;
    
    temp1.push_back(x1);
    temp1.push_back(y1);
    
    temp2.push_back(x2);
    temp2.push_back(y2);
    
    points.push_back(temp1);
    points.push_back(temp2);
    
    return predictNextPoint(points);
}

vector<double> predictNextPoint(vector<vector<double> > &prevPoints){
	vector<double> nextPoint;
	// find previous two points
	if(prevPoints.size()<2){
		cout << "Cannot calculate next point, too few data" << endl;
		return nextPoint;
	}

	double curr_x = prevPoints[prevPoints.size()-1][0];
	double curr_y = prevPoints[prevPoints.size()-1][1];
	double prev_x = prevPoints[prevPoints.size()-2][0];
	double prev_y = prevPoints[prevPoints.size()-2][1];
	
	// direction (basic vector math), i.e. prev_x+d_x = curr_x
	double d_x = curr_x-prev_x;
	double d_y = curr_y-prev_y;

	double next_x = curr_x + d_x;
	double next_y = curr_y + d_y;
	
	nextPoint.push_back(next_x);
	nextPoint.push_back(next_y);

	return nextPoint;
}

vector<float> calculateCentroid(vector<vector<float> > &points){
	float txpoints = 0;
	float typoints = 0;
	for (int counter = 0; counter < points.size(); counter++){
		txpoints += points[counter][0];
		typoints += points[counter][1];
	}
	vector<float> centroid;
	centroid.push_back(txpoints / points.size());
	centroid.push_back(typoints / points.size());
	return centroid;
}
vector<double> calculateCentroid(vector<double> &xcoordinates, vector<double> &ycoordinates){

	double txpoints = 0;
	double typoints = 0;

    vector<double> centroid;

	for (int counter = 0; counter < xcoordinates.size(); counter++){
		txpoints += xcoordinates[counter];
		typoints += ycoordinates[counter];
	}

	centroid.emplace_back(txpoints / xcoordinates.size());
	centroid.emplace_back(typoints / ycoordinates.size());

	return centroid;
}
