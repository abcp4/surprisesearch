[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Surprise Search 

Source code for Surprise Search 

## Publication:

Gravina, Daniele, Antonios Liapis, and Georgios N. Yannakakis. “Surprise Search for Evolutionary Divergence” arXiv preprint arXiv:1706.02556 (2017).

## Requirements

C++11 compiler.

## IDE

Source code has been written and tested with CLion 2017.2.3

## Domain

Mazes used in the experiments are available at https://gitlab.com/2factor/SurpriseMazes

## Contact:

Daniele Gravina (daniele.gravina@um.edu.mt)

## Additional Notes

The provided source code is based on the original novelty search source code provided by Joel Lehman and Kenneth O. Stanley [1].

## References

[1] Lehman, Joel, and Kenneth O. Stanley. "Abandoning objectives: Evolution through the search for novelty alone." Evolutionary computation 19.2 (2011): 189-223.

----
